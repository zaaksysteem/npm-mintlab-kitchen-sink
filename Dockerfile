ARG WORKSPACE=/home/node/app/package

#### Stage: update npm and create working directory
FROM node:carbon-alpine AS base
ARG WORKSPACE
RUN npm install -g npm@6\
 && mkdir -p $WORKSPACE\
 && chown -R node:node $WORKSPACE

#### Stage: copy files
FROM base AS files
ARG WORKSPACE
WORKDIR $WORKSPACE
# copy dot and package files to root
COPY --chown=node:node ./root/* ./
COPY --chown=node:node ./root ../root
COPY --chown=node:node ./source ./source

#### Stage: install dependencies
FROM files AS install
USER node
RUN npm install --no-optional --loglevel error

#### Stage: set up development environment
FROM install AS development
RUN rm ./package*.json\
 && ln -s ../root/package.json ./package.json\
 && ln -s ../root/package-lock.json ./package-lock.json
