const dictionary = require('./dictionary');

/**
 * Create a dictionary that is not enumerable.
 *
 * @param {Object} object
 * @return {Object}
 */
const expose = object =>
  dictionary(object, {
    enumerable: false,
  });

module.exports = expose;
