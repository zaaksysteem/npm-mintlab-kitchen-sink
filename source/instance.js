const expose = require('./expose');
const { assign } = Object;

/**
 * Bind an infinite set of methods to a class instance.
 *
 * @param {Object} instance
 *   Instance of a class
 * @param {...string} methods
 *   Method names of a class
 */
function bind(instance, ...methods) {
  const reduceBoundMethods = (accumulator, name) =>
    assign(accumulator, {
      [name]: instance[name].bind(instance),
    });
  const boundMethods = methods.reduce(reduceBoundMethods, {});

  assign(instance, boundMethods);
}

module.exports = expose({
  bind,
});
