const test = require('tape');
const dictionary = require('./dictionary');

const { keys } = Object;

test('dictionary()', assert => {
  {
    const actual = dictionary({
      foo: 'bar',
    });
    const expected = {
      foo: 'bar',
    };
    const message = 'passes the given properties';

    assert.deepEqual(actual, expected, message);
  }
  {
    const actual = dictionary({
      foo: 'bar',
    });
    const expected = 'bar';
    const message = 'sets readonly properties';

    actual.foo = 'VALUE';
    assert.equal(actual.foo, expected, message);
  }
  {
    const { length: actual } = keys(
      dictionary({
        foo: 'bar',
      })
    );
    const expected = 1;
    const message = 'sets enumerable properties';

    assert.equal(actual, expected, message);
  }
  assert.end();
});
