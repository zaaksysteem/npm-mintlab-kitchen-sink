// Modules exposed in the entry file are for DLL pre-builds.
// What you include here is your own judgement call.
const {
  asArray,
  removeFromArray,
  isPopulatedArray,
  toggleItem,
} = require('./array');
const dictionary = require('./dictionary');
const expose = require('./expose');
const unique = require('./unique');
const { callOrNothingAtAll, passOrGet } = require('./function');
const { bind } = require('./instance');
const { reduceMap } = require('./iterable');
const {
  buildParamString,
  buildUrl,
  getSegment,
  getSegments,
  objectifyParams,
} = require('./url');
const {
  cloneWithout,
  extract,
  filterProperties,
  filterPropertiesOnValues,
  get,
  getObject,
  isObject,
  performGetOnProperties,
  purge,
} = require('./object');
const { capitalize } = require('./string');

module.exports = expose({
  asArray,
  bind,
  buildParamString,
  buildUrl,
  callOrNothingAtAll,
  capitalize,
  cloneWithout,
  dictionary,
  expose,
  extract,
  filterProperties,
  filterPropertiesOnValues,
  get,
  getObject,
  getSegment,
  getSegments,
  isObject,
  isPopulatedArray,
  objectifyParams,
  passOrGet,
  performGetOnProperties,
  purge,
  reduceMap,
  removeFromArray,
  toggleItem,
  unique,
});
