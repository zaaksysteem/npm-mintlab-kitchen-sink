const expose = require('./expose');

const capitalize = string =>
  string.replace(/^./, function Upper(match) {
    return match.toUpperCase();
  });

module.exports = expose({
  capitalize,
});
