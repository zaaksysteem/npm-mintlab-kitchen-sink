const expose = require('./expose');
const { isArray } = Array;

/**
 * @param {*} value
 * @return {Array}
 */
function asArray(value) {
  if (isArray(value)) {
    return value;
  }

  return [value];
}

/**
 * Mutate a given array by removing a given element.
 *
 * @param {Array} array
 *   The array to mutate.
 * @param {*} value
 *   The element to remove.
 */
function removeFromArray(array, value) {
  const index = array.indexOf(value);
  const length = 1;

  array.splice(index, length);
}

/**
 * @param {Array} array
 * @return {boolean}
 */
function isPopulatedArray(array) {
  return Boolean(isArray(array) && array.length);
}

function toggleItem(array, item) {
  if(array.includes(item)) {
    return array.filter(thisId => thisId !== item);
  }

  return [...array, item];
}

module.exports = expose({
  asArray,
  removeFromArray,
  isPopulatedArray,
  toggleItem,
});
