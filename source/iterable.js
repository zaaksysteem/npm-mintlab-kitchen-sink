const { passOrGet } = require('./function');
const expose = require('./expose');

/**
 * Iterate a `Map` of key/value functions until the first *key*
 * that produces a truthy result when called and return the outcome
 * of calling the associated *value*.
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map
 *
 * @param {Object} options
 * @param {Map} options.map
 * @param {Array} [options.keyArguments=[]]
 * @param {Array} [options.valueArguments=[]]
 * @param {*} [options.fallback]
 * @return {*}
 */
function reduceMap({
  map,
  keyArguments = [],
  valueArguments = [],
  fallback,
}) {
  for (const [mapKey, mapValue] of map) {
    if (mapKey(...keyArguments)) {
      return mapValue(...valueArguments);
    }
  }

  return passOrGet(fallback, valueArguments);
}

module.exports = expose({
  reduceMap,
});
