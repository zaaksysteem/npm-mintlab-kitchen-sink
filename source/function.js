const expose = require('./expose');

/**
 * Get the return value of the argument if
 * it is a function or pass it through.
 *
 * @param {*} value
 * @param {Array} [argumentList]
 * @return {*}
 */
function passOrGet(value, argumentList = []) {
  if (typeof value === 'function') {
    return value(...argumentList);
  }

  return value;
}

/**
 * Call a value with optional arguments if it is a function.
 *
 * @param {*} callback
 *   A value that is called if it is a function.
 * @param {Array|Function} [argumentList=[]]
 *   The optional arguments for the callback.
 * @returns {*}
 *   The return value of the function if it is called or undefined.
 */
function callOrNothingAtAll(callback, argumentList = []) {
  if (typeof callback === 'function') {
    return callback(...passOrGet(argumentList));
  }
}

module.exports = expose({
  callOrNothingAtAll,
  passOrGet,
});
